package ru.t1.amsmirnov.taskmanager.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import java.util.List;

public interface ProjectClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @GetMapping("/findAll")
    List<ProjectWebDto> findAll();

    @GetMapping("/findById/{id}")
    ProjectWebDto findById(@PathVariable("id") final String id);

    @PostMapping("/save")
    ProjectWebDto save(@RequestBody final ProjectWebDto project);

    @PostMapping("/create")
    ProjectWebDto create(
            @RequestParam("name") String name,
            @RequestParam(value = "description", defaultValue = "") String desc
    );

    @DeleteMapping("/delete")
    ProjectWebDto delete(@RequestBody final ProjectWebDto project);

    @DeleteMapping("/delete/{id}")
    ProjectWebDto delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    void deleteAll();
}
