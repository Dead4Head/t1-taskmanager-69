package ru.t1.amsmirnov.taskmanager.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectDtoService projectService;

    @GetMapping("/projects")
    public ModelAndView projects(
            @AuthenticationPrincipal final CustomUser user
    ) {
        try {
            return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
        } catch (Exception e) {
            final ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error");
            modelAndView.addObject("error", e);
            return modelAndView;
        }
    }

}
