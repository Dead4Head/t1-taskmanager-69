package ru.t1.amsmirnov.taskmanager.controller.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.amsmirnov.taskmanager.dto.CustomUser;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.service.dto.ProjectDtoService;

@Controller
public class ProjectController {

    @Autowired
    private ProjectDtoService projectService;

    @PostMapping("/project/create")
    public ModelAndView create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        try {
            projectService.create(user.getUserId(), "Project_" + System.currentTimeMillis(), "");
            return new ModelAndView("redirect:/projects");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/project/delete/{id}")
    public ModelAndView delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        try {
            projectService.removeOneById(user.getUserId(), id);
            return new ModelAndView("redirect:/projects");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        try {
            final ProjectWebDto project = projectService.findOneById(user.getUserId(), id);
            if (project == null)
                return new ModelAndView("redirect:/projects");
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("project-edit");
            modelAndView.addObject("project", project);
            modelAndView.addObject("statuses", Status.values());
            return modelAndView;
        } catch (Exception e) {
            return errorView(e);
        }
    }

    @PostMapping("/project/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") ProjectWebDto project,
            BindingResult result
    ) {
        try {
            projectService.update(user.getUserId(), project);
            return new ModelAndView("redirect:/projects");
        } catch (Exception e) {
            return errorView(e);
        }
    }

    private ModelAndView errorView(final Exception e) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("error", e);
        return modelAndView;
    }

}
