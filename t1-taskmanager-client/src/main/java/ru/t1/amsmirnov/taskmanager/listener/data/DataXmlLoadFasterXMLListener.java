package ru.t1.amsmirnov.taskmanager.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlLoadFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlLoadFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataXmlLoadFasterXMLListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataXmlLoadFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXMLRequest request = new DataXmlLoadFasterXMLRequest(getToken());
        @NotNull final DataXmlLoadFasterXMLResponse response = domainEndpoint.loadDataXmlFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
