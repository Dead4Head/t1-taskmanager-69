package ru.t1.amsmirnov.taskmanager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectChangeStatusByIdResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.event.ConsoleEvent;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

@Component
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change project status by ID.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS: ");
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, status);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
