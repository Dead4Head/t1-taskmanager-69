package ru.t1.amsmirnov.taskmanager.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.amsmirnov.taskmanager.listener.LoggerListener;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

@Configuration
@ComponentScan("ru.t1.amsmirnov.taskmanager")
public class LoggerConfiguration {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

    @Bean
    public MessageListener listener() {
        return new LoggerListener();
    }

}
