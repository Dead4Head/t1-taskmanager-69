package ru.t1.amsmirnov.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.api.service.model.IService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;
import ru.t1.amsmirnov.taskmanager.repository.model.AbstractRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractModelService<M extends AbstractModel, R extends AbstractRepository<M>>
        implements IService<M> {

    @NotNull
    protected R repository;

    public AbstractModelService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> addAll(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null) throw new ModelNotFoundException();
        removeAll();
        addAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        return repository.findAll(getComparator(comparator));
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        return model.get();
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new ModelNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        repository.delete(model.get());
        return model.get();
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public int getSize() throws AbstractException {
        return findAll().size();
    }

    @Override
    public boolean existById(@NotNull final String id) throws AbstractException {
        return repository.existsById(id);
    }

    protected Sort getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return Sort.by("status", "created");
        if (comparator instanceof NameComparator) return Sort.by("name", "created");
        return Sort.by("created");
    }

}
