package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataBinSaveResponse extends AbstractResultResponse {

    public DataBinSaveResponse() {
    }

    public DataBinSaveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
